package uebung2;

//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStreamWriter;

public class LZW {

	
	
	public static void main(String[] argv){
//		BufferedReader br = null;
//		String file = "";
//		try {
//			br = new BufferedReader(new FileReader("midsummer.txt"));
//			String line;
//			while((line = br.readLine()) != null){
//				file += line + "\n";
//			}
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		String testString = "/WED/WE/WEE/WEB/WET";
//		String testString = "wabba wabba wabba wabba woo woo woo";
		LZWEncode encode = new LZWEncode(testString);
		
		System.out.println("Initiales Wörterbuch: " + encode.getInit_wb());
		System.out.println("Wörterbuch: " + encode.getWb());
		System.out.println("Codewort:" + encode.getCodewort());
		System.out.println(encode.getCodewort().size());
//		String foo = "";
//		for(Integer i: encode.getCodewort()){
//			foo += i + " ";
//		}
//		File code = new File("code.txt");
//		try{
//			FileWriter writer = new FileWriter(code,false);
//			writer.write(foo);
//			writer.flush();
//			writer.close();
//			
//		} catch (IOException e) {
//		      e.printStackTrace();
//	    }
		//Ausgabe durch WB
		String output = "";
		for(Integer x :encode.getCodewort()){
			output += encode.getWb().get(x);
		}
		System.out.println(output);
		
		LZWDecode decode = new LZWDecode(encode.getCodewort(), encode.getInit_wb());
		
		System.out.println(decode.getMessage());
	}
}
