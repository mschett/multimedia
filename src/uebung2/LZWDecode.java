package uebung2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LZWDecode {
	
	private final List<Integer> Codewort;
	private Map<Integer, String> wb;
	
	private String message = "";
	
	public String getMessage() {
		return message;
	}

	public LZWDecode(List<Integer> Codewort, Map<Integer, String> init_wb) {
		this.Codewort = Codewort;
		this.wb = new HashMap<Integer, String>(init_wb);
		this.decode();
	}
	
	private void decode(){
		Integer actualCodeword = Codewort.get(0);
		message += wb.get(actualCodeword);

		Integer nextCodeword;
		for(int i = 1; i < Codewort.size(); i++){
			nextCodeword = Codewort.get(i);
			if(wb.containsKey(nextCodeword)){
				wb.put(wb.size(), (
						wb.get(actualCodeword) + wb.get(nextCodeword).substring(0, 1)
						));
				message += wb.get(nextCodeword);
			} else {
				wb.put(wb.size(), (
						wb.get(actualCodeword) + wb.get(actualCodeword).substring(0, 1)
						));
				message += wb.get(actualCodeword) + wb.get(actualCodeword).substring(0, 1);
			}
			actualCodeword = nextCodeword;
		}
	}

}
