package uebung2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class LZWEncode {

	final String input;
	private Map<Integer, String> wb;
	private Map<Integer, String> init_wb = new HashMap<>();
	
	private List<Integer> Codewort = new LinkedList<>();

	public Map<Integer, String> getInit_wb() {
		return init_wb;
	}

	public Map<Integer, String> getWb() {
		return wb;
	}

	public List<Integer> getCodewort() {
		return Codewort;
	}

	public LZWEncode(String input){
		this.input = input;
		this.encode();
	}
	
	private void encode(){
		String actualString;
		//Wörterbuch initialisieren
		for(int i = 0; i < input.length(); i++){
			actualString = input.substring(i, i + 1);
			if(!init_wb.containsValue(actualString)){
				init_wb.put(init_wb.size(), actualString);
			}
		}
		
		sortWB();
		
		this.wb = new HashMap<>(this.init_wb);
		//Restkodierung
		String nextChar;
		actualString = input.substring(0, 1);
		for(int i = 1; i < input.length(); i++){
			nextChar = input.substring(i, i + 1);
			if(wb.containsValue(actualString + nextChar)){
				
				actualString = actualString + nextChar;
				
			} else {
				Codewort.add(this.getKey(actualString)); //Key for s
				wb.put(wb.size(), actualString + nextChar);
				actualString = nextChar;
				
			}
		}
		Codewort.add(this.getKey(actualString)); //Key for s
	}
	
	private Integer getKey(String value){
		for(Integer i: wb.keySet()){
			if(wb.get(i).equals(value))
				return i;
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private void sortWB(){
		List<Entry<Integer, String>> toSort = new ArrayList<>(this.init_wb.entrySet());
		Collections.sort(toSort,
				new Comparator<Object>() {

					@Override
					public int compare(Object o1, Object o2) {
						Entry<Integer, String> e1 = (Entry<Integer, String>) o1;
						Entry<Integer, String> e2 = (Entry<Integer, String>) o2;
						return  e1.getValue().compareTo(e2.getValue());
					}
			
		});
		this.init_wb.clear();
		for(Entry<Integer, String> x: toSort){
			this.init_wb.put(this.init_wb.size(), x.getValue());
		}
	}
}
